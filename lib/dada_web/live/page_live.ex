defmodule DadaWeb.PageLive do
  use DadaWeb, :live_view

  alias Dada.{Repo, LinkMapping}

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     assign(socket,
       full_link: "",
       shortened_link: "",
       error_message: nil,
       previous_submission: "",
       show_result: false,
       disabled: true
     )}
  end

  @impl true
  def handle_event("shorten", %{"fl" => full_link}, socket) do
    case Repo.get_by(LinkMapping, full_link: full_link) do
      nil ->
        link_mapping = get_link_mapping(full_link)

        {:noreply,
         assign(socket,
           shortened_link: LinkMapping.get_shortened_link(link_mapping),
           previous_submission: full_link,
           show_result: true,
           disabled: true
         )}

      link_mapping ->
        {:noreply,
         assign(socket,
           shortened_link: LinkMapping.get_shortened_link(link_mapping),
           previous_submission: full_link,
           show_result: true,
           disabled: true
         )}
    end
  end

  @impl true
  def handle_event("validate", %{"fl" => full_link}, socket) do
    trimmed = String.trim(full_link)

    if String.length(trimmed) < 1 do
      {:noreply, assign(socket, error_message: nil, disabled: true)}
    else
      if Dada.is_valid_uri?(trimmed) do
        {:noreply, assign(socket, error_message: nil, disabled: false)}
      else
        {:noreply, assign(socket, error_message: "Please enter a valid URL", disabled: true)}
      end
    end
  end

  @impl true
  def handle_event("hide_result", _assigns, socket) do
    {:noreply, assign(socket, show_result: false)}
  end

  defp get_link_mapping(full_link) do
    case Repo.get_by(LinkMapping, full_link: full_link) do
      nil ->
        case create_link_mapping(full_link) do
          {:ok, link_mapping} -> link_mapping
          {:error, _} -> get_link_mapping(full_link)
        end

      link_mapping ->
        link_mapping
    end
  end

  defp create_link_mapping(full_link) do
    Repo.insert(LinkMapping.new(full_link))
  end

end
