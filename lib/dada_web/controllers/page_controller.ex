defmodule DadaWeb.PageController do
  use DadaWeb, :controller

  alias Dada.{Repo, LinkMapping}


  def route(conn, %{"slug" => slug}) do
    case Repo.get_by(LinkMapping, slug: slug) do
      nil ->
        conn
        |> put_status(:not_found)
        |> put_view(DadaWeb.ErrorView)
        |> render(:"404")
      link_mapping ->
        conn
        |> redirect(external: link_mapping.full_link)
    end
  end
end
