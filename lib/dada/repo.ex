defmodule Dada.Repo do
  use Ecto.Repo,
    otp_app: :dada,
    adapter: Ecto.Adapters.Postgres
end
