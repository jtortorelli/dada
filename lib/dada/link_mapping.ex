defmodule Dada.LinkMapping do
  use Ecto.Schema
  import Ecto.Changeset
  alias Dada.LinkMapping

  schema "link_mappings" do
    field :full_link, :string
    field :slug, :string

    timestamps()
  end

  @doc false
  def changeset(link_mapping, attrs) do
    link_mapping
    |> cast(attrs, [:full_link, :slug])
    |> validate_required([:full_link, :slug])
    |> unique_constraint(:full_link)
    |> unique_constraint(:slug)
  end

  def new(full_link) do
    %LinkMapping{full_link: full_link, slug: generate_slug()}
  end

  defp generate_slug() do
    length = Application.get_env(:dada, :slug_length)
    
    :crypto.strong_rand_bytes(length)
    |> Base.url_encode64()
    |> binary_part(0, length)
  end

  def get_shortened_link(%LinkMapping{slug: slug}) do
    "#{Application.get_env(:dada, :env_host)}/#{slug}"
  end
end
