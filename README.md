# Dada

A miniature webapp for shortening web urls.

Use the Makefile instructions:

  * `make setup` - builds Docker image, fetches mix dependencies, installs npm dependencies
  * `make server` - applies ecto migrations and starts the phoenix server
  * `make test` - executes tests

