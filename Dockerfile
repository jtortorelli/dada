FROM elixir:latest

RUN apt-get update

RUN curl -fsSl https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs

COPY . /app
WORKDIR /app

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get
RUN mix compile
RUN npm install --prefix assets

ADD entrypoint.sh .

CMD ./entrypoint.sh
