# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :dada,
  ecto_repos: [Dada.Repo],
  env_host: "http://localhost:4000",
  slug_length: 8

# Configures the endpoint
config :dada, DadaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "STw7cbVcLjifp8BZm5QxM/heIoNtbxA3bWVdHQhHLUrgj1IP3lls45WzkQSYoHcb",
  render_errors: [view: DadaWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Dada.PubSub,
  live_view: [signing_salt: "pVkbluAX"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
