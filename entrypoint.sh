#!/usr/bin/env bash

mix ecto.create
mix ecto.migrate

exec mix phx.server
