defmodule DadaWeb.PageLiveTest do
  use DadaWeb.ConnCase

  import Phoenix.LiveViewTest

  test "loads page with initial state", %{conn: conn} do
    {:ok, view, _} = live(conn, "/")
    assert view |> element("#micronize") |> render() =~ "disabled=\"disabled\""
  end

  test "handles empty field", %{conn: conn} do
    {:ok, view, html} = live(conn, "/")

    # initially has no error messaging and no warning triangle, micronize button is disabled
    refute html =~ "Please enter a valid URL"
    refute has_element?(view, ".fa-exclamation-triangle")
    assert view |> element("#micronize") |> render() =~ "disabled=\"disabled\""

    # does not render any error messaging on empty field
    refute render_change(view, :validate, %{"fl" => ""}) =~ "Please enter a valid URL"
    refute has_element?(view, ".fa-exclamation-triangle")
    assert view |> element("#micronize") |> render() =~ "disabled=\"disabled\""
  end

  test "handles blank field", %{conn: conn} do
    {:ok, view, html} = live(conn, "/")

    # initially has no error messaging and no warning triangle, micronize button is disabled
    refute html =~ "Please enter a valid URL"
    refute has_element?(view, ".fa-exclamation-triangle")
    assert view |> element("#micronize") |> render() =~ "disabled=\"disabled\""

    # does not render any error messaging on blank field
    refute render_change(view, :validate, %{"fl" => "    "}) =~ "Please enter a valid URL"
    refute has_element?(view, ".fa-exclamation-triangle")
    assert view |> element("#micronize") |> render() =~ "disabled=\"disabled\""
  end

  test "handles invalid url", %{conn: conn} do
    {:ok, view, html} = live(conn, "/")

    # initially has no error messaging and no warning triangle, micronize button is disabled
    refute html =~ "Please enter a valid URL"
    refute has_element?(view, ".fa-exclamation-triangle")
    assert view |> element("#micronize") |> render() =~ "disabled=\"disabled\""

    # on submission of invalid url, error messaging and warning triangle are present, micronize button is still disabled
    assert render_change(view, :validate, %{"fl" => "blarg"}) =~ "Please enter a valid URL"
    assert view |> element("#micronize") |> render() =~ "disabled=\"disabled\""
    assert has_element?(view, ".fa-exclamation-triangle")
  end

  test "handles valid url", %{conn: conn} do
    {:ok, view, _} = live(conn, "/")

    # initially micronize button is disabled
    assert view |> element("#micronize") |> render() =~ "disabled=\"disabled\""

    # on submission of valid url, micronize button is enabled
    render_change(view, :validate, %{"fl" => "http://google.com"})
    refute view |> element("#micronize") |> render() =~ "disabled=\"disabled\""
  end

  test "displays modal", %{conn: conn} do
    {:ok, view, _} = live(conn, "/")

    # initially modal is not active
    refute view |> element(".modal") |> render() =~ "is-active"

    render_click(view, :shorten, %{"fl" => "http://google.com"})

    # on submission of valid url, modal should be active
    assert view |> element(".modal") |> render() =~ "is-active"

    # should display shortened url
    assert view
           |> element("#shortened")
           |> render()
           |> String.match?(
             ~r/#{Application.get_env(:dada, :host)}\/[[:alnum:][:punct:]]{#{
               Application.get_env(:dada, :slug_length)
             }}/
           )

    # on submission of valid url, form should disable again (prep for next submission)
    assert view |> element("#micronize") |> render() =~ "disabled=\"disabled\""
  end

  test "returns same shortened url for same full url", %{conn: conn} do
    {:ok, view, _} = live(conn, "/")

    render_click(view, :shorten, %{"fl" => "http://google.com"})
    shortened = view |> element("#shortened") |> render()

    render_click(view, :shorten, %{"fl" => "http://google.com"})
    assert view |> element("#shortened") |> render() == shortened
  end

  test "returns different shortened url for different full url", %{conn: conn} do
    {:ok, view, _} = live(conn, "/")

    render_click(view, :shorten, %{"fl" => "http://google.com"})
    shortened = view |> element("#shortened") |> render()

    render_click(view, :shorten, %{"fl" => "http://bing.com"})
    refute view |> element("#shortened") |> render() == shortened
  end
end
