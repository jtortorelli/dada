defmodule DadaWeb.PageControllerTest do
  use DadaWeb.ConnCase

  alias Dada.Repo
  alias Dada.LinkMapping

  setup do
    link_mapping = Repo.insert!(%LinkMapping{full_link: "http://google.com", slug: "nERelilV"})
    %{link_mapping: link_mapping}
  end

  test "redirects to full url when slug is found in DB", %{conn: conn, link_mapping: link_mapping} do
    conn = get(conn, Routes.page_path(conn, :route, link_mapping.slug))
    assert redirected_to(conn) == link_mapping.full_link
  end

  test "routes to 404 when slug is not found in DB", %{conn: conn} do
    conn = get(conn, Routes.page_path(conn, :route, "blarg"))
    assert html_response(conn, 404) =~ "This page was not found"
  end
end
