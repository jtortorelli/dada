defmodule Dada.LinkMappingTest do
  use ExUnit.Case
  alias Dada.LinkMapping

  test "creates new link mapping" do
    created = LinkMapping.new("http://google.com")
    assert created.full_link == "http://google.com"

    assert String.match?(
             created.slug,
             ~r/[[:alnum:][:punct:]]{#{Application.get_env(:dada, :slug_length)}}/
           )
  end

  test "gets shortened url" do
    created = LinkMapping.new("http://google.com")
    shortened_url = LinkMapping.get_shortened_link(created)
    assert shortened_url == "#{Application.get_env(:dada, :env_host)}/#{created.slug}"
  end
end
