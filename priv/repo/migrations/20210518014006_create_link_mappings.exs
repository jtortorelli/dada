defmodule Dada.Repo.Migrations.CreateLinkMappings do
  use Ecto.Migration

  def change do
    create table(:link_mappings) do
      add :full_link, :string
      add :shortened_link, :string

      timestamps()
    end

    create unique_index(:link_mappings, [:full_link])
  end
end
