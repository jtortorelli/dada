defmodule Dada.Repo.Migrations.AddHashColumnToLinkMappingTable do
  use Ecto.Migration

  def change do
    alter table("link_mappings") do
      add :hash, :string
    end
    create unique_index(:link_mappings, [:hash])
  end
end
