defmodule Dada.Repo.Migrations.SetUniqueShortenedLink do
  use Ecto.Migration

  def change do
    create unique_index(:link_mappings, [:shortened_link])
  end
end
