defmodule Dada.Repo.Migrations.RefactorLinkMappingTable do
  use Ecto.Migration

  def change do
    alter table("link_mappings") do
      remove :shortened_link
    end
    rename table("link_mappings"), :hash, to: :slug
  end
end
